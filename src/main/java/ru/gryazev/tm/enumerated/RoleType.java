package ru.gryazev.tm.enumerated;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.Nullable;

@AllArgsConstructor
public enum RoleType {

    USER("User"),
    ADMIN("Administrator");

    @Nullable private final String roleName;

    @Nullable
    public String displayName(){
        return roleName;
    }

}
