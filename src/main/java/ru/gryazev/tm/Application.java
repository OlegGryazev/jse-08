package ru.gryazev.tm;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.project.*;
import ru.gryazev.tm.command.system.AboutCommand;
import ru.gryazev.tm.command.system.ExitCommand;
import ru.gryazev.tm.command.system.HelpCommand;
import ru.gryazev.tm.command.task.*;
import ru.gryazev.tm.command.user.*;
import ru.gryazev.tm.context.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        @NotNull final Class[] commandClasses = new Class[]{ExitCommand.class, HelpCommand.class, AboutCommand.class,
                ProjectClearCommand.class, ProjectCreateCommand.class, ProjectEditCommand.class, ProjectListCommand.class,
                ProjectRemoveCommand.class, ProjectSelectCommand.class, ProjectViewCommand.class, TaskClearCommand.class,
                TaskClearCommand.class, TaskCreateCommand.class, TaskEditCommand.class, TaskListCommand.class,
                TaskRemoveCommand.class, TaskViewCommand.class, TaskUnlinkCommand.class, TaskLinkCommand.class,
                TaskListUnlinkedCommand.class, UserLoginCommand.class, UserLogoutCommand.class,
                UserRegistrationCommand.class, UserUpdateCommand.class, UserViewCommand.class, UserEditCommand.class,
                UserRemoveCommand.class, UserListCommand.class};
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(commandClasses);
    }

}
