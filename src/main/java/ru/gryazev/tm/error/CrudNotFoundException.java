package ru.gryazev.tm.error;

public final class CrudNotFoundException extends RuntimeException {

    public CrudNotFoundException() {
        super("Error: element not found.");
    }

}
