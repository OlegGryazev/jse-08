package ru.gryazev.tm.error;

public final class CrudDeleteException extends RuntimeException {

    public CrudDeleteException() {
        super("Error during delete operation");
    }

}
