package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.entity.Project;

@AllArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @Getter
    @NotNull private final IProjectRepository repository;

    @Nullable
    public String getProjectId(final int projectIndex, @Nullable final String userId){
        if (userId == null || userId.isEmpty() || projectIndex < 0) return null;
        Project project = repository.findProjectByIndex(userId, projectIndex);
        if (project == null) return null;
        return project.getId();
    }

    @Nullable
    @Override
    public Project findByName(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectName == null || projectName.isEmpty()) return null;
        return repository.findByUserIdAndName(userId, projectName);
    }

    @Override
    public boolean isEntityValid(@Nullable final Project project) {
        if (project == null) return false;
        if (project.getId() == null || project.getId().isEmpty()) return false;
        if (project.getName() == null || project.getName().isEmpty()) return false;
        return project.getUserId() != null && !project.getUserId().isEmpty();
    }

}
