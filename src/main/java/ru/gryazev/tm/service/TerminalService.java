package ru.gryazev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.ITerminalService;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.util.DateUtils;
import ru.gryazev.tm.util.HashUtils;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public final class TerminalService implements ITerminalService {

    @NotNull private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @NotNull
    public Task getTaskFromConsole() throws IOException {
        @NotNull final Task task = new Task();
        System.out.println("[Enter name:]");
        task.setName(reader.readLine());
        System.out.println("[Enter details:]");
        task.setDetails(reader.readLine());
        @Nullable final Date startDate = getDateFromConsole();
        task.setDateStart(startDate);
        @Nullable final Date endDate = getDateFromConsole();
        task.setDateFinish(endDate);
        return task;
    }

    @NotNull
    public Project getProjectFromConsole() throws IOException{
        @NotNull final Project project = new Project();
        System.out.println("[Enter name:]");
        project.setName(reader.readLine());
        System.out.println("[Enter details:]");
        project.setDetails(reader.readLine());
        @Nullable final Date startDate = getDateFromConsole();
        project.setDateStart(startDate);
        @Nullable final Date endDate = getDateFromConsole();
        project.setDateFinish(endDate);
        return project;
    }

    @NotNull
    public User getUserFromConsole() throws IOException {
        @NotNull final User user = new User();
        System.out.println("[Enter login:]");
        user.setLogin(reader.readLine());
        user.setPwdHash(getPwdHashFromConsole());
        user.setRoleType(RoleType.USER);
        return user;
    }

    @Nullable
    public User getUserPwdRepeat() throws IOException {
        @NotNull final User user = getUserFromConsole();
        @NotNull final String pwdRepeat = getPwdHashFromConsole();
        if (!pwdRepeat.equals(user.getPwdHash())){
            print("Entered passwords do not match!");
            return null;
        }
        return user;
    }

    @NotNull
    public String getPwdHashFromConsole() throws IOException {
        System.out.println("[Enter password:]");
        @Nullable final String pwdHash = HashUtils.MD5(reader.readLine());
        if (pwdHash == null)
            throw new IOException("Error during password input.");
        return pwdHash;
    }

    public int getTaskIndex() {
        return inputNumber("task index");
    }

    public int getProjectIndex() {
        return inputNumber("project index");
    }

    public int getUserIndex() {
        return inputNumber("user index");
    }

    @NotNull
    public String readCommand() throws IOException {
        return reader.readLine();
    }

    @Override
    public void close(){
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void print(@Nullable final String str){
        System.out.println(str);
    }

    private int inputNumber(@Nullable final String source) {
        final int id;
        System.out.println(String.format("[Enter %s:]", source));
        try {
            id = Integer.parseInt(reader.readLine());
        } catch (NumberFormatException | IOException e) {
            System.out.println(String.format("[Entered %s is incorrect.]", source));
            return -1;
        }
        return id - 1;
    }

    @Nullable
    private Date getDateFromConsole() throws IOException {
        System.out.println("[Enter date of start in DD.MM.YYYY format:]");
        @Nullable Date date = DateUtils.formatStringToDate(reader.readLine());
        if (date == null) {
            System.out.println("Entered date is incorrect. You can edit date later.");
            return null;
        }
        return date;
    }

}
