package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractRepository<T extends AbstractCrudEntity> implements IRepository<T> {

    @NotNull protected final Map<String, T> entities = new LinkedHashMap<>();

    @Nullable
    @Override
    public T findOne(@NotNull final String userId, @NotNull final String id) {
        @Nullable final T t = entities.get(id);
        if (t == null || !userId.equals(t.getUserId())) return null;
        return t;
    }

    @Nullable
    @Override
    public T merge(@NotNull final String userId, @NotNull final T t) {
        if (!userId.equals(t.getUserId())) return null;
        entities.put(t.getId(), t);
        return t;
    }

    @NotNull
    @Override
    public Map<String, T> findAll(@NotNull final String userId) {
        @NotNull final Map<String, T> result = new LinkedHashMap<String , T>();
        entities.forEach((k, v) -> {
            if (userId.equals(v.getUserId())) result.put(v.getId(), v);
        });
        return result;
    }

    @Nullable
    @Override
    public T persist(@NotNull final String userId, @NotNull final T t) {
        if (!userId.equals(t.getUserId())) return null;
        if (findOne(userId, t.getId()) != null) return null;
        entities.put(t.getId(), t);
        return t;
    }

    @Nullable
    @Override
    public T remove(@NotNull final String userId, @NotNull final String id) {
        if (!userId.equals(entities.get(id).getUserId())) return null;
        return entities.remove(id);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final List<T> tsToRemove = entities.values().stream().filter(o ->
                userId.equals(o.getUserId())).collect(Collectors.toList());
        tsToRemove.forEach(o -> entities.remove(o.getId()));
    }

}
