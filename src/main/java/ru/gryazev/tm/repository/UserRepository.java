package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLoginAndPwd(@NotNull final String login, @NotNull final String pwd) {
        return entities.values().stream().filter(o ->
                login.equals(o.getLogin()) && pwd.equals(o.getPwdHash())).findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return new ArrayList<User>(entities.values());
    }

    @Nullable
    @Override
    public User findUserByIndex(final int userIndex) {
        return entities.values().stream().skip(userIndex).findFirst().orElse(null);
    }

}
