package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.entity.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Nullable
    @Override
    public Project findByUserIdAndName(@NotNull final String userId, @NotNull final String projectName) {
        return entities.values().stream().filter(o ->
                userId.equals(o.getUserId()) && projectName.equals(o.getName()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Project findProjectByIndex(@NotNull final String userId, final int index) {
        return entities.values().stream().filter(o -> userId.equals(o.getUserId()))
                .skip(index).findFirst().orElse(null);
    }

}
