package ru.gryazev.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.service.IStateService;
import ru.gryazev.tm.api.service.ITerminalService;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.service.TerminalService;

import java.io.IOException;

@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    @Nullable protected ServiceLocator serviceLocator;

    @Nullable protected ITerminalService terminalService;

    @Nullable protected IStateService stateService;

    @Getter
    private boolean allowed = false;

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws IOException;

    public RoleType[] getRoles() {
        return null;
    }

}
