package ru.gryazev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks of selected project.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @Nullable final String projectId = stateService.getCurrentProjectId();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().listTaskByProject(stateService.getCurrentUserId(), projectId);
        if (tasks.size() == 0) throw new CrudListEmptyException();
        for (int i = 0; i < tasks.size(); i++)
            terminalService.print((i + 1) + ". " + tasks.get(i).getName());
    }

}
