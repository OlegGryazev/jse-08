package ru.gryazev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudCreateException;
import ru.gryazev.tm.error.CrudNotFoundException;

import java.io.IOException;

public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task at selected project.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @Nullable final String userId = stateService.getCurrentUserId();
        terminalService.print("[TASK CREATE]");

        @NotNull final Task task = terminalService.getTaskFromConsole();
        @Nullable String currentProjectId = stateService.getCurrentProjectId();
        if (currentProjectId == null) currentProjectId = serviceLocator.getProjectService()
                .getProjectId(terminalService.getProjectIndex(), userId);
        if (currentProjectId == null) throw new CrudNotFoundException();
        task.setUserId(userId);
        task.setProjectId(currentProjectId);
        @Nullable final Task createdTask = serviceLocator.getTaskService().create(userId, task);
        if (createdTask == null) throw new CrudCreateException();
        terminalService.print("[OK]");
    }

}
