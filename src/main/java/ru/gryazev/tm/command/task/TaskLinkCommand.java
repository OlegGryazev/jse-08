package ru.gryazev.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudUpdateException;

public final class TaskLinkCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-link";
    }

    @Override
    public String getDescription() {
        return "Link selected task to project.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @Nullable final String userId = stateService.getCurrentUserId();
        @Nullable final String currentProjectId = stateService.getCurrentProjectId();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = serviceLocator.getProjectService()
                .getProjectId(projectIndex, stateService.getCurrentUserId());
        final int taskIndex = terminalService.getTaskIndex();

        @Nullable final Task linkedTask = serviceLocator.getTaskService()
                .linkTask(userId, currentProjectId, projectId, taskIndex);
        if (linkedTask == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
