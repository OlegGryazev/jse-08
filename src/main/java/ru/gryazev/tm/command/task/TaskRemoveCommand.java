package ru.gryazev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudDeleteException;
import ru.gryazev.tm.error.CrudNotFoundException;

public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task from selected project.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final String userId = stateService.getCurrentUserId();
        final int taskIndex = terminalService.getTaskIndex();
        @Nullable String currentProjectId = stateService.getCurrentProjectId();
        if (currentProjectId == null) currentProjectId = serviceLocator.getProjectService()
                .getProjectId(terminalService.getProjectIndex(), userId);
        if (currentProjectId == null) throw new CrudNotFoundException();
        @Nullable final String taskId = taskService.getTaskId(currentProjectId, userId, taskIndex);

        @Nullable final Task removedTask = taskService.remove(userId, taskId);
        if (removedTask == null) throw new CrudDeleteException();
        terminalService.print("[DELETED]");
    }

}
