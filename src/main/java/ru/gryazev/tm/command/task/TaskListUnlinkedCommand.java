package ru.gryazev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.util.List;

public final class TaskListUnlinkedCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-list-unlinked";
    }

    @Override
    public String getDescription() {
        return "Shows all unlinked tasks.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().listTaskUnlinked(stateService.getCurrentUserId());
        if (tasks.size() == 0) throw new CrudListEmptyException();
        for (int i = 0; i < tasks.size(); i++)
            terminalService.print((i + 1) + ". " + tasks.get(i).getName());
    }

}
