package ru.gryazev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

import java.io.IOException;

public final class TaskEditCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected task.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final String userId = stateService.getCurrentUserId();
        @Nullable String currentProjectId = stateService.getCurrentProjectId();
        if (currentProjectId == null) currentProjectId = serviceLocator.getProjectService()
                .getProjectId(terminalService.getProjectIndex(), userId);
        if (currentProjectId == null) throw new CrudNotFoundException();
        final int taskIndex = terminalService.getTaskIndex();
        @Nullable final String taskId = taskService.getTaskId(currentProjectId, userId, taskIndex);
        if (taskId == null) throw new CrudNotFoundException();

        @NotNull final Task task = terminalService.getTaskFromConsole();
        task.setId(taskId);
        task.setProjectId(currentProjectId);
        task.setUserId(userId);
        @Nullable final Task editedTask = taskService.edit(userId, task);
        if (editedTask == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
