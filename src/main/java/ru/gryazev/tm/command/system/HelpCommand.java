package ru.gryazev.tm.command.system;

import ru.gryazev.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    public HelpCommand() {
        setAllowed(true);
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        serviceLocator.getStateService().getCommands().forEach((k, v) -> terminalService.print(v.getName() + ": " + v.getDescription()));
    }

}
