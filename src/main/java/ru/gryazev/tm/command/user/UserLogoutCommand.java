package ru.gryazev.tm.command.user;

import ru.gryazev.tm.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "Logout from Project Manager.";
    }

    @Override
    public void execute() {
        if (terminalService == null || stateService == null) return;
        if (stateService.isUserLogged()) {
            stateService.setCurrentUserId(null);
            terminalService.print("You are logged out.");
        }
    }

}
