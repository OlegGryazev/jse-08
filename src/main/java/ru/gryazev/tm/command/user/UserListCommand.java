package ru.gryazev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;

public class UserListCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-list";
    }

    @Override
    public String getDescription() {
        return "Shows all users (REQUIRES ADMIN ROLE).";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null) return;
        for (@NotNull final User user : serviceLocator.getUserService().findAll())
            terminalService.print(user.getLogin());
    }

    @Override
    public RoleType[] getRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

}
