package ru.gryazev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.User;

import java.io.IOException;

public final class UserLoginCommand extends AbstractCommand {

    public UserLoginCommand() {
        setAllowed(true);
    }

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "User authorization.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @NotNull final User user = terminalService.getUserFromConsole();
        @Nullable final String userId = serviceLocator.getUserService().login(user);
        if (userId == null) {
            terminalService.print("[ERROR DURING USER LOGIN]");
            return;
        }
        terminalService.print("[" + user.getLogin() + " logged in]");
        stateService.setCurrentUserId(userId);
    }

}
