package ru.gryazev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.error.CrudNotFoundException;

public class UserRemoveCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected user (REQUIRES ADMIN ROLE).";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final IUserService userService = serviceLocator.getUserService();
        final int userIndex = terminalService.getUserIndex();
        @Nullable final String userId = userService.getUserId(userIndex);
        if (userId == null) throw new CrudNotFoundException();
        userService.remove(userId, userId);
        terminalService.print("[OK]");
    }

    @Override
    public RoleType[] getRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

}
