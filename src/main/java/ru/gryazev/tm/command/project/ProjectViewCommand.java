package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudNotFoundException;

public final class ProjectViewCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-view";
    }

    @Override
    public String getDescription() {
        return "View selected project.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable final String userId = stateService.getCurrentUserId();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = projectService.getProjectId(projectIndex, userId);

        @Nullable final Project project = projectService.findOne(userId, projectId);
        if (project == null) throw new CrudNotFoundException();
        terminalService.print(project.toString());
    }

}
