package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudDeleteException;

public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable final String userId = stateService.getCurrentUserId();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = projectService.getProjectId(projectIndex, userId);

        @Nullable final Project removedProject = projectService.remove(userId, projectId);
        if (removedProject == null) throw new CrudDeleteException();
        serviceLocator.getTaskService().removeByProjectId(userId, projectId);
        terminalService.print("[DELETED]");
    }

}
