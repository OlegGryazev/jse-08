package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

import java.io.IOException;

public final class ProjectEditCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || stateService == null || terminalService == null) return;
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable final String userId = stateService.getCurrentUserId();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = projectService.getProjectId(projectIndex, stateService.getCurrentUserId());
        if (projectId == null) throw new CrudNotFoundException();

        @NotNull final Project project = terminalService.getProjectFromConsole();
        project.setId(projectId);
        project.setUserId(userId);
        @Nullable final Project editedProject = projectService.edit(userId, project);
        if (editedProject == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
