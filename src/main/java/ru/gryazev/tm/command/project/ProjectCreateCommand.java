package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudCreateException;

import java.io.IOException;

public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || stateService == null || terminalService == null) return;
        @Nullable final String userId = stateService.getCurrentUserId();
        terminalService.print("[PROJECT CREATE]");

        @NotNull final Project project = terminalService.getProjectFromConsole();
        project.setUserId(userId);
        @Nullable final Project createdProject = serviceLocator.getProjectService().create(userId, project);
        if (createdProject == null) throw new CrudCreateException();
        terminalService.print("[OK]");
    }

}
