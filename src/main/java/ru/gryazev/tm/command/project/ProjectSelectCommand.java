package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;

public final class ProjectSelectCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-select";
    }

    @Override
    public String getDescription() {
        return "Select project. To reset, type \"-1\".";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = serviceLocator.getProjectService()
                .getProjectId(projectIndex, stateService.getCurrentUserId());
        stateService.setCurrentProjectId(projectId);
    }

}
