package ru.gryazev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtils {

    @Nullable
    public static Date formatStringToDate(@Nullable String dateString){
        @NotNull final SimpleDateFormat format = new SimpleDateFormat("d.MM.yyyy");
        Date result;
        try {
            result = format.parse(dateString);
        } catch (ParseException e){
            return null;
        }
        return result;
    }

    @NotNull private static final SimpleDateFormat dateFormat = new SimpleDateFormat("d.MM.yyyy");

    @NotNull
    public static String formatDateToString(@Nullable final Date date){
        return date != null ? dateFormat.format(date) : "undefined";
    }

}
