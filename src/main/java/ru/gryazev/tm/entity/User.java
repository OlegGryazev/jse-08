package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.enumerated.RoleType;

@Getter
@Setter
public final class User extends AbstractCrudEntity {

    @Nullable private String login = "";

    @Nullable private String pwdHash = "";

    @Nullable private RoleType roleType;

    public User() {
        setUserId(getId());
    }

    @NotNull
    @Override
    public String toString() {
        return "Login: " + login + "\n" +
                "Role: " + roleType;
    }

}
