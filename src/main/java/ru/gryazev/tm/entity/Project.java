package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.util.DateUtils;

import java.util.Date;

@Getter
@Setter
public final class Project extends AbstractCrudEntity {

    @Nullable private String name = "";

    @Nullable private String details = "";

    @Nullable private Date dateStart;

    @Nullable private Date dateFinish;

    @NotNull
    @Override
    public String toString() {
        return String.format("Name: %s\n" +
                        "Details: %s\n" +
                        "Starts: %s\n" +
                        "Ends: %s",
                name,
                details,
                DateUtils.formatDateToString(dateStart),
                DateUtils.formatDateToString(dateFinish));
    }

}
