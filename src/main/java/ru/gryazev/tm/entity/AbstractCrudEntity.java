package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCrudEntity {

    @NotNull private String id = UUID.randomUUID().toString();

    @Nullable private String userId = null;

}
