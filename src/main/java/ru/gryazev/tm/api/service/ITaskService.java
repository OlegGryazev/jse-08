package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @NotNull
    public List<Task> listTaskUnlinked(@Nullable final String userId);

    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId);

    @Nullable
    public Task unlinkTask(@Nullable final String userId, @Nullable final Task task);

    @Nullable
    public String getTaskId(@Nullable final String projectId, @Nullable final String userId, final int taskIndex);

    @NotNull
    public List<Task> listTaskByProject(@Nullable final String userId, @Nullable final String projectId);

    @Nullable
    public Task linkTask(@Nullable final String userId,
                         @Nullable final String oldProjectId,
                         @Nullable final String newProjectId,
                         final int taskIndex);

}
