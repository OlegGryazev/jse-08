package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    public String login(@Nullable final User user);

    public boolean checkRole(@Nullable final String userId, @Nullable final RoleType[] roles);

    @NotNull
    public List<User> findAll();

    @Nullable
    public String getUserId(final int userIndex);

}
