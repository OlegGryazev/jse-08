package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.entity.User;

import java.io.Closeable;
import java.io.IOException;

public interface ITerminalService extends Closeable {

    @NotNull
    public Task getTaskFromConsole() throws IOException;

    @NotNull
    public Project getProjectFromConsole() throws IOException;

    @NotNull
    public User getUserFromConsole() throws IOException;

    @Nullable
    public User getUserPwdRepeat() throws IOException;

    @NotNull
    public String getPwdHashFromConsole() throws IOException;

    public int getTaskIndex();

    public int getProjectIndex();

    public int getUserIndex();

    @NotNull
    public String readCommand() throws IOException;

    @Override
    public void close();

    public void print(@Nullable final String str);

}
