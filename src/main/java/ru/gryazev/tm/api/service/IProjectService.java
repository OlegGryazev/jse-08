package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Project;

public interface IProjectService extends IService<Project> {

    @Nullable
    public String getProjectId(final int projectIndex, @Nullable final String userId);

    @Nullable
    public Project findByName(@Nullable final String userId, @Nullable final String projectName);

}
