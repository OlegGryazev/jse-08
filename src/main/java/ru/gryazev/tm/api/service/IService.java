package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.util.List;

public interface IService<T extends AbstractCrudEntity> {

    @Nullable
    public T create(@Nullable final String userId, @Nullable final T t);

    @Nullable
    public T findOne(@Nullable final String userId, @Nullable final String entityId);

    @NotNull
    public List<T> findByUserId(@Nullable final String userId);

    @Nullable
    public T edit(@Nullable final String userId, @Nullable final T t);

    @Nullable
    public T remove(@Nullable final String userId, @Nullable final String entityId);

    public void clear(@Nullable final String userId);

    @NotNull
    public IRepository<T> getRepository();

}
