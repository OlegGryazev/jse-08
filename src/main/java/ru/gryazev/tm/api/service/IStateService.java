package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;

import java.util.Map;

public interface IStateService {

    public boolean isUserLogged();

    @Nullable
    public String getCurrentUserId();

    @Nullable
    public String getCurrentProjectId();

    @NotNull
    public Map<String, AbstractCommand> getCommands();

    public void setCurrentUserId(@Nullable final String currentUserId);

    public void setCurrentProjectId(@Nullable final String currentProjectId);

    public void setCommands(@NotNull final Map<String, AbstractCommand> commands);

}
