package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    public User findByLoginAndPwd(@NotNull final String login, @NotNull final String pwd);

    @NotNull
    public List<User> findAll();

    @Nullable
    public User findUserByIndex(final int userIndex);

}
