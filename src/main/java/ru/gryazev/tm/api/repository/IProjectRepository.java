package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Project;

public interface IProjectRepository extends IRepository<Project> {

    @Nullable
    public Project findByUserIdAndName(@NotNull final String userId, @NotNull final String projectName);

    @Nullable
    public Project findProjectByIndex(@NotNull final String userId, final int index);

}
