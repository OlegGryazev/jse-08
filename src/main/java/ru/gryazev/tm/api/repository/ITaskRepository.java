package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    public List<Task> findTasksByProjectId(@NotNull final String userId, @NotNull final String projectId);

    @NotNull
    public List<Task> findUnlinked(@NotNull final String userId);

    @Nullable
    public Task findTaskByIndex(@NotNull final String projectId, @NotNull final String userId, final int taskIndex);

    @Nullable
    public Task findUnlinkedTaskByIndex(@NotNull final String userId, final int taskIndex);

}
