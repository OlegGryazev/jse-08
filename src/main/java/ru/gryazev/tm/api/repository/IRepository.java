package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.util.Map;

public interface IRepository<T extends AbstractCrudEntity> {

    @Nullable
    public T findOne(@NotNull final String userId, @NotNull final String id);

    @Nullable
    public T merge(@NotNull final String userId, @NotNull final T t);

    @NotNull
    public Map<String, T> findAll(@NotNull final String userId);

    @Nullable
    public T persist(@NotNull final String userId, @NotNull final T t);

    @Nullable
    public T remove(@NotNull final String userId, @NotNull final String id);

    public void removeAll(@NotNull final String userId);

}
