#### Project Manager

###### Remote repository: 
https://gitlab.com/OlegGryazev/jse-08
###### Software requirements:
* JDK 8
* Apache Maven 3.6.3
    
###### Technology stack:
* Maven
* Java SE
    
###### Developer:
    Gryazev Oleg
    email: gryazev77@gmail.com
    
###### Build:
    mvn clean install
 
###### Run:
    java -jar target/projectmanager/bin/projectmanager.jar